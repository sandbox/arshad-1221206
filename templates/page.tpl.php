<?php
/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<div id="page-wrapper">
	<div id="header">
		<div class="section clearfix">
			<div id="site-name">
				<a href="<?php print $front_page; ?>"><?php print $site_name; ?></a>
			</div> <!-- /#site-name -->
			<div id="navigation">
				<?php if ($main_menu) : ?>
					<?php print theme('links__system_main_menu', array(
						'links' => $main_menu,
						'attributes' => array(
							'id' => 'main-menu-links',
							'class' => array('links', 'clearfix'),
						),
					)); ?>
				<?php endif; ?>
			</div>
			<div id="search-form-wrapper">
				<?php print drupal_render($search_form); ?>
			</div>
		</div>
	</div><!-- /header-->
		
	<div id="main" class="clearfix">
		<?php if($page['pictor_slideshow'] && $is_front): ?>
			<div id="slideshow">
				<?php print drupal_render($page['pictor_slideshow']); ?>
			</div><!-- slideshow-->
		<?php endif; ?>
		
		<?php if($page['pictor_tagline'] && $is_front): ?>
			<div id="tagline">
				<?php print drupal_render($page['pictor_tagline']); ?>
			</div><!-- tagline-->
		<?php endif; ?>
		
		<?php if ($breadcrumb): ?>
			<div id="breadcrumb"><?php print $breadcrumb; ?></div>
		<?php endif; ?>
		
		<a name="main-content"></a>
		<div id="content">
			<?php print $messages; ?>
		
			<?php print render($title_prefix); ?>
			<?php if ($title): ?>
				<h1 class="title" id="page-title">
					<?php print $title; ?>
				</h1>
			<?php endif; ?>
			<?php print render($title_suffix); ?>
			<?php if ($tabs): ?>
				<div class="tabs">
					<?php print render($tabs); ?>
				</div>
			<?php endif; ?>
			<?php print render($page['help']); ?>
			<?php if ($action_links): ?>
				<ul class="action-links">
					<?php print render($action_links); ?>
				</ul>
			<?php endif; ?>
			<?php print render($page['pictor_content']); ?>
			<?php print $feed_icons; ?>
		</div> <!-- /#content -->
		
		<?php if ($page['pictor_sidebar']) : ?>
			<div id="sidebar">
				<?php print render($page['pictor_sidebar']); ?>
			</div>
		<?php endif; ?>
			
	</div><!-- main -->
	
	<?php if($page['pictor_footer']): ?>
		<div id="footer">
			<?php print render($page['pictor_footer']); ?>
		</div><!-- footer-->
	<?php endif; ?>
		
</div><!-- page-wrapper -->