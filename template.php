<?php

/**
 * @file
 * Theme functions for Pictor
 */

/**
 * Implements template_preprocess_page().
 */
function pictor_preprocess_page(&$variables) {
  //inject the search form
  $search_form = drupal_get_form('search_form');
  $variables['search_form'] = $search_form;
}